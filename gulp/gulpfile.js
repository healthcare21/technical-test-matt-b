var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var sass = require('gulp-sass');
let cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
const ftp = require( 'vinyl-ftp' );
const sftp = require('gulp-sftp');
var  connect  = require('gulp-connect-php');

gulp.task('php', function() {
    php.server({ base: 'app', port: 8010, keepalive: true});
});

var slack = require('gulp-slack')({
    url: 'https://hooks.slack.com/services/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',
    icon_emoji: ':bowtie:' // Optional
});

// task for deploying files on the server
gulp.task('deploy', function() {
    const config = require('./sftp-config.json');

    const globs = [
        '../**/*',
        '../*'
    ];

    if (config.type == 'ftp') {
        //  FTP version
        const conn = ftp.create( {
            host:     config.host,
            user:     config.user,
            password: config.password,
            port:     config.port,
            parallel: 10,
            reload:   true,
            debug:    function(d){console.log(d);}
        });
        return gulp.src( globs, { base: '../', buffer: false } )
            .pipe( conn.newer( '/hc21-new.hc21staging.co.uk/web/app/themes/healthcare21/' ) ) // only upload newer files
            .pipe( conn.dest( '/hc21-new.hc21staging.co.uk/web/app/themes/healthcare21/' ) );
    } else {
        // SFTP version
        const conn = sftp({
                host: config.host,
                user: config.user,
                pass: config.password,
                port: config.port,
                remotePath: config.remote_path,
            });
            // gulp.src('app/**/*').pipe(conn)
        return gulp.src('app/**/*' )
            .pipe(conn)
            .pipe(slack('Deployed latest build Gulp Test'));
    }
});



gulp.task('sass', function() {
  gulp.src('../scss/*.scss')
  .pipe(sass({style: 'expanded'}))
  .pipe(gulp.dest('../css'))
});

//Minify CSS
gulp.task('minify-css', () => {
  console.log('minify');
  return gulp.src('../css/*.css')
  .pipe(cleanCSS({debug: true}, (details) => {
    console.log(`${details.name}: ${details.stats.originalSize}`);
    console.log(`${details.name}: ${details.stats.minifiedSize}`);
  }))
  .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('../css/min/'));
});

gulp.task('browserSync', function() {
  connect.server({}, function (){
    browserSync.init({
      proxy: 'http://healthcare-21-website.test/'
    });
  })
})


gulp.task('browserSync_html', function() {
  browserSync.init({
    server: {
      baseDir: '../'
    },
  })
})

gulp.task('useref', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulp.dest('dist'))
});


gulp.task('watch',['browserSync_html'], function(){
  gulp.watch('../css/*.css', ['minify-css']);
  gulp.watch('../scss/*.scss', ['sass']);
  gulp.watch('../scss/__partials/*.*', ['sass']);
  gulp.watch("../*.html").on('change', browserSync.reload);
  gulp.watch("../css/*.css").on('change', browserSync.reload);
  // gulp.watch("app/*.php").on('change', browserSync.reload);
  // gulp.watch("../*.css").on('change', browserSync.reload);
  // gulp.watch('app/assets/js/*.js', browserSync.reload);
  // Other watchers
})
