# Technical test.
Time taken: 55mins for basic layout - no rollovers.  
Technologies used -  
HTML  
CSS  
SCSS  
Gulp  
  
Currently no javascript  
Gulp file has deployment script and FTP/SFTP setup  
SCSS broken down into partials as follows:  
layout.scss - main layout file for the main page  
mixins.scss - mixing for border radius.  
variables.scss - colour variables only.  
  
## Install Gulp  
Change directory to gulp folder:  
`cd gulp`  

Install dependancies  
`npm install`  
  
Run gulp watch   
`gulp watch`  
  
## Improvements  
Use calc functions to determine width  
Make the ticket responsive?  
Add rollovers / Javascript messages  
Take the value of the input and subtract from total and change the red bar location and the arrow location.  
Add share functionality  
Add bookmark functionality to save for later.  

